<?php
/* 
* CallForwardingAlways script v1.2 voor Broadsoft XSI-Actions
* -------------------------------------------------------
*
* CallForwardingAlways script v1.2 voor Broadsoft XSI-Actions is in licentie gegeven volgens een 
* Creative Commons Naamsvermelding-NietCommercieel-GeenAfgeleideWerken 4.0 Internationaal-licentie.
* 
* Human Readable : https://creativecommons.org/licenses/by-nc-nd/4.0/deed.nl
* Full License : https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.nl
*
*
*
Onderstaande gegevens aanpassen !
$authUser 			=> Groepsadmin van Broadsoft
$authPass 			=> Wachtwoord Groepsadmin Broadsoft
$userId 			=> userid (van extensie) waarvan de service moet worden aangepast
$profile 			=> 'services/callforwardingalways'; Wil zeggen dat de functie CFA gebruikt wordt, deze niet aanpassen!
*/

$authUser = '';
$authPass = '';
$userId = '';
$profile = 'services/callforwardingalways';
$base_uri = 'http://xsp.voipit.nl/com.broadsoft.xsi-actions/v2.0/user/' . $userId . '/';

/* 
Onderstaand GEEN gegevens aanpassen!!!
======================================
*/

require 'vendor/autoload.php';
use Guzzlehttp\Client;
$jar = new \GuzzleHttp\Cookie\CookieJar;

$Gclient = new GuzzleHttp\Client([
    'base_uri' => $base_uri,
]);

if (isset($_GET['number'])) {
	$number = $_GET['number'];
}

if (isset($_GET['active'])) {
	$active = $_GET['active'];
}

if (isset($number) && isset($active)) {

	$CFA_xml = <<<EOF
	<?xml version="1.0" encoding="ISO-8859-1"?>
	<CallForwardingAlways xmlns="http://schema.broadsoft.com/xsi">
	<active>$active</active>
	<forwardToPhoneNumber>$number</forwardToPhoneNumber>
	<ringSplash>false</ringSplash>
	</CallForwardingAlways>
EOF;

	try {
		$Gresponse = $Gclient->request('PUT', $profile,[
		'body' => $CFA_xml,
	   	'auth' => [$authUser, $authPass],
	   	'cookies' => $jar
	]);
		
	} catch (Exception $e) {
		echo "<h2>Error!</h2><p>$e</p>";
	}

}

/*
* Bovenstaand geen gegevens aanpassen!
*
* Pas hieronder de knoppen configuratie aan voor het toestel.
* Verander het nummer naar het nummer van de doorschakeling en verander ExecuteItem Uri naar een geweste knop/kleur
*/

if ($number == "200"){
echo '<?xml version="1.0"?>';
echo '<YealinkIPPhoneExecute>';
echo '<ExecuteItem URI="Led:POWER=on"/>';
echo '<ExecuteItem URI="Led:LINE5_GREEN=on"/>';
echo '<ExecuteItem URI="Led:LINE6_GREEN=off"/>';
echo '<ExecuteItem URI="Led:LINE7_GREEN=off"/>';
echo '</YealinkIPPhoneExecute>';
}elseif ($number == "201"){
echo '<?xml version="1.0"?>';
echo '<YealinkIPPhoneExecute>';
echo '<ExecuteItem URI="Led:POWER=off"/>';
echo '<ExecuteItem URI="Led:LINE5_GREEN=off"/>';
echo '<ExecuteItem URI="Led:LINE6_GREEN=on"/>';
echo '<ExecuteItem URI="Led:LINE7_GREEN=off"/>';
echo '</YealinkIPPhoneExecute>';	
}
elseif ($number == "202"){
echo '<?xml version="1.0"?>';
echo '<YealinkIPPhoneExecute>';
echo '<ExecuteItem URI="Led:POWER=off"/>';
echo '<ExecuteItem URI="Led:LINE5_GREEN=off"/>';
echo '<ExecuteItem URI="Led:LINE6_GREEN=off"/>';
echo '<ExecuteItem URI="Led:LINE7_GREEN=on"/>';
echo '</YealinkIPPhoneExecute>';
}
else {
	echo 'No Valid Number!';
}
?>