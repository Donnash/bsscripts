<?php
/* 
* disableCallWaiting script v1 voor Broadsoft XSI-Actions
* -------------------------------------------------------
*  Door DJA Doomen
* -------------------------------------------------------
*
* disableCallWaiting script v1 voor Broadsoft XSI-Actions is in licentie gegeven volgens een 
* Creative Commons Naamsvermelding-NietCommercieel-GeenAfgeleideWerken 4.0 Internationaal-licentie.
* 
* Human Readable : https://creativecommons.org/licenses/by-nc-nd/4.0/deed.nl
* Full License : https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode.nl
*
*
*
Onderstaande gegevens aanpassen !
$authUser 			=> Groepsadmin van Broadsoft
$authPass 			=> Wachtwoord Groepsadmin Broadsoft
$users 				=> Array van user ID's in Broadsoft
*/

require 'vendor/autoload.php';

// AANPASSEN !!
$authUser = 'xxxxx@xxxxx.bsuep.kpn1.nl';
$authPass = 'xxxxx';
$users = array(
"xxxx@xxxx.bsuep.kpn1.nl",
"xxxx@xxxx.bsuep.kpn1.nl"
);
// NIETS AANPASSEN VANAF HIER !!

use Guzzlehttp\Client;
$jar = new \GuzzleHttp\Cookie\CookieJar;

//Create Guzzle Client
$Gclient = new GuzzleHttp\Client([
	'base_uri' => 'https://cai.voipit.nl/nl.cai.xsi-actions/v2.0/user/'
]);

$callWaitingOff = <<<EOF
<?xml version="1.0" encoding="ISO-8859-1"?>
<CallWaiting xmlns="http://schema.broadsoft.com/xsi">
<active>false</active>
</CallWaiting>
EOF;

foreach ($users as $user) {
	try {
		$Gresponse = $Gclient->request('PUT', $user.'/services/callwaiting',[
			'body' => $callWaitingOff,
			'auth' => [$authUser, $authPass],
			'cookies' => $jar
		    ]);
		echo $Gresponse->getReasonPhrase();
		} 
	catch (Exeption $e) {
			echo $e->getResponse()->getReasonPhrase();
		}
		echo '<br>';
}

?>